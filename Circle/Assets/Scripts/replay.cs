﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class replay : MonoBehaviour
{
  // Start is called before the first frame update
  void Start()
  { 
  }

  // Update is called once per frame
  void Update()
  {
      
  }

  public void onClick(){
    //On set le score du joueur à 0
    gameState.Instance.setScorePlayer(0);
    //On relance la partie
    SceneManager.LoadScene("game");
  }

}
