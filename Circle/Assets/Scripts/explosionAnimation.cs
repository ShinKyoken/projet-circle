﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class explosionAnimation : MonoBehaviour
{
  // Start is called before the first frame update
  void Start()
  {
   //Au bout de 2 seconde, on lance la methode finAnimation
   InvokeRepeating("finAnimation", 2.0f, 2.0f);   
  }

  // Update is called once per frame
  void Update()
  {
  }

  void finAnimation(){
    //On détruit l'explosion
    Destroy(gameObject);
    //On set le highscore correspondant à la difficulté
    gameState.Instance.setHighScore(gameStateDifficulty.Instance.getDifficulty());
    //On lance la scene de fin
    SceneManager.LoadScene("end");
  }
}
