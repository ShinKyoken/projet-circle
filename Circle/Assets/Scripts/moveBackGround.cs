﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBackGround : MonoBehaviour {
	private float sizeX;
	private float positionRestartX;
	private Vector2 movement;
	private Vector3 basGauche;
	// Use this for initialization
	void Start () {
		//La position x de l'objet dont le tag est b2
		positionRestartX = GameObject.FindGameObjectWithTag("b2").transform.position.x - 0.1f;
		//On bouge le background vers la gauche avec une vitesse de 2
		movement = new Vector2(-2, 0);
		basGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
		GetComponent<Rigidbody2D>().velocity = movement;
		sizeX = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
	}
	
	void Update(){
		//Lorsque le background sort de l'écran, il va à la position positionRestartX. Cela permet de faire une boucle.
		if (transform.position.x < basGauche.x - (sizeX / 2)){
			transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		}
	}
}
