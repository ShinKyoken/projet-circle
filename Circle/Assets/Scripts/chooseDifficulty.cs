﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class chooseDifficulty : MonoBehaviour
{
  private int level;

  // Start is called before the first frame update
  void Start()
  {
      
  }

  // Update is called once per frame
  void Update()
  {
      
  }

  public void onClick(){
    if(GameObject.FindWithTag("difficulty").GetComponent<Text>().text == "Facile"){
      level = 1;
    }
    else if(GameObject.FindWithTag("difficulty").GetComponent<Text>().text == "Normal"){
      level = 2;
    }
    else{
      level = 3;
    }
    gameStateDifficulty.Instance.setDifficulty(level);
    gameState.Instance.setScorePlayer(0);
    SceneManager.LoadScene("game");
  }
}
