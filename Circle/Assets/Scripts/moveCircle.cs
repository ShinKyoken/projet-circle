﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCircle : MonoBehaviour
{
  private float sizeY;
  private float sizeX;
  private float speed;
  private Vector2 movement;
  private Vector3 basGauche;
  private Vector3 hautDroite;
  private int rand;

  // Start is called before the first frame update
  void Start()
  {
    //Bouge le cercle vers la droite avec une vitesse de 2
    setSpeed(2.0f);
    movement = new Vector2(speed, 0);
    GetComponent<Rigidbody2D>().velocity = movement;
    sizeY = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
    sizeX = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
    basGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
    hautDroite = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));      
  }

  // Update is called once per frame
  void Update()
  {
    rand = Random.Range(0,3);

    //En fonction du bord avec lequel le cercle est entré en collision, il y a 3 possibilté pour la direction du rebond
    if(transform.position.y > hautDroite.y/2 - sizeY){
      if(rand == 0){
        movement = new Vector2(0,-speed).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
      else if(rand == 1){
        movement = new Vector2(-speed,-speed).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
      else{
        movement = new Vector2(speed,-speed).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
    }
    else if(transform.position.y < basGauche.y/2 + sizeY ){
      if(rand == 0){
        movement = new Vector2(0,speed).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
      else if(rand == 1){
        movement = new Vector2(-speed,speed).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
      else{
        movement = new Vector2(speed,speed).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
    }
    else if(transform.position.x < basGauche.x + sizeX ){
      if(rand == 0){
        movement = new Vector2(speed,0).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
      else if(rand == 1){
        movement = new Vector2(speed,speed).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
      else{
        movement = new Vector2(speed,-speed).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
    }
    else if(transform.position.x > hautDroite.x - sizeX){
      if(rand == 0){
        movement = new Vector2(-speed,0).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
      else if(rand == 1){
        movement = new Vector2(-speed,-speed).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
      else{
        movement = new Vector2(-speed,speed).normalized;
        GetComponent<Rigidbody2D>().velocity = movement * 2;
      }
    }
  }

  public float getSpeed(){
    return speed;
  }

  public void setSpeed(float vitesse){
    speed = vitesse;
  }

  public void upSpeed(float up){
    speed += up;
  }
}
