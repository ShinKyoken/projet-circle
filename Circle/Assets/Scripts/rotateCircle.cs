using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateCircle : MonoBehaviour {

	private Vector3 myRotation;

	// Use this for initialization
	void Start () {
	}

	void spin() {
		//On tourne le cercle de 4 degrès vers la droite
		myRotation.z -= 4.0f ;
		transform.rotation = Quaternion.Euler(myRotation);
 	}

	
	// Update is called once per frame
	void Update () {
		//Quand on presse n'importe quel bouton
		if (Input.anyKey){
			spin();
		}
		
	}

}