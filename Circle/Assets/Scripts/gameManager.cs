using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameManager : MonoBehaviour
{ 
  public GameObject asteroid;
  public GameObject life_facile;
  public GameObject life_normal;
  public GameObject life_difficile;
  public Sprite red_asteroid;
  public Sprite blue_asteroid;
  private GameObject clone_asteroid;
  private Vector3 basGauche;
  private Vector3 basDroite;
  private Vector3 hautGauche;
  private float lastTime;
  private float time;
  private float timeBetweenAsteroid = 3.0f;
  private int color;
  private int random_position;
  private bool isPause = true;
  private GameObject vie;

    // Start is called before the first frame update
    void Start()
    {
      basGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
      basDroite = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
      hautGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
      color = Random.Range(0,2);
      
      //On instancie les objet point de vie en fonction de la difficulté
      if(gameStateDifficulty.Instance.getDifficulty() == 1){
        vie = Instantiate(life_facile, new Vector3(basGauche.x + 0.45f, hautGauche.y - 0.7f , 0), Quaternion.identity) as GameObject;
      }
      else if(gameStateDifficulty.Instance.getDifficulty() == 2){
        vie = Instantiate(life_normal, new Vector3(basGauche.x + 0.45f, hautGauche.y - 0.7f , 0), Quaternion.identity) as GameObject;
      }
      else{
        vie = Instantiate(life_difficile, new Vector3(basGauche.x + 0.45f, hautGauche.y - 0.7f , 0), Quaternion.identity) as GameObject;
      }

      //On lance countdown
      StartCoroutine(countdown());

      //Toute les 2 sec on lance la methode setTimeBetweenAsteroid
      InvokeRepeating("upDifficulty", 2.0f, 2.0f);
    }

    // Update is called once per frame
    void Update(){

      //On instancie des asteroides venant du haut et du bas de l'écran. Ils sont soit rouge, soit bleu.
      time = Time.time;
      if ((time - lastTime > timeBetweenAsteroid) && !isPause){
        random_position = Random.Range(0,2);
        if(random_position == 0){
          clone_asteroid = Instantiate(asteroid, new Vector3(Random.Range(basGauche.x,basDroite.x), hautGauche.y, 0), Quaternion.identity) as GameObject;
          clone_asteroid.AddComponent<moveAsteroidDown>();
        }
        else if(random_position == 1){
          clone_asteroid = Instantiate(asteroid, new Vector3(Random.Range(basGauche.x,basDroite.x), basGauche.y, 0), Quaternion.identity) as GameObject;
          clone_asteroid.AddComponent<moveAsteroidUp>();
        }
        
        if(color == 0){
          clone_asteroid.GetComponent<SpriteRenderer>().sprite = red_asteroid;
          color = 1;
        }
        else if(color == 1){
          clone_asteroid.GetComponent<SpriteRenderer>().sprite = blue_asteroid;  
          color = 0;
        }
        lastTime = time;
      }
    }

    private void upDifficulty(){
      //On diminue le temps entre chaque apparition d'astéroides.
      if(timeBetweenAsteroid>1.1f){
        timeBetweenAsteroid -= 0.10f;
      }
      else if(GameObject.FindWithTag("circle").GetComponent<moveCircle>().getSpeed() < 3.0f){
        Debug.Log("up speed");
        GameObject.FindWithTag("circle").GetComponent<moveCircle>().upSpeed(0.1f);
      } 
    }

    IEnumerator countdown(){
      //On fait un compte à rebours
      yield return new WaitForSeconds(1);
      GameObject.FindWithTag("countdown").GetComponent<Text>().text = "" + 2;

      yield return new WaitForSeconds(1);
      GameObject.FindWithTag("countdown").GetComponent<Text>().text = "" + 1;

      yield return new WaitForSeconds(1);
      GameObject.FindWithTag("countdown").GetComponent<Text>().text = "" + 0;
      
      yield return new WaitForSeconds(0.5f);
      Destroy(GameObject.FindWithTag("countdown"));

      //A la fin du compte à rebours, on donne les scripts
      GameObject.FindWithTag("circle").AddComponent<moveCircle>();
      GameObject.FindWithTag("circle").AddComponent<rotateCircle>();
      GameObject.FindWithTag("circle").AddComponent<addScore>();

      GameObject.FindWithTag("b1").AddComponent<moveBackGround>();
      GameObject.FindWithTag("b2").AddComponent<moveBackGround>();

      //On permet au GameManager d'instancier des asteroides
      isPause = false;

    }

}